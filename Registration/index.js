function createBusinessAccount() {

    // container
    let regContainer = document.createElement('div');
    regContainer.classList.add('registrationContainer');

    // header
    let regConHeader = document.createElement('h2');
    regConHeader.classList.add('registrationHeader');
    regConHeader.innerText = "Mağaza məlumatları";

    // form
    let regBusinessForm = document.createElement('form');
    regBusinessForm.classList.add('registrationForm');

    // inputs
    for (let index = 0; index < 3; index++) {
        let regBusinessInput = document.createElement('input');
        regBusinessInput.classList.add('registrationInput');
        regBusinessInput.type = "text";

        if(index == 0) {
            regBusinessInput.placeholder = "Dükan adı";
        }
        else if (index == 1) {
            regBusinessInput.placeholder = "Dükan adresi";
        }
        else {
            regBusinessInput.placeholder = "Əlaqə nömrəsi";
        }

        regBusinessForm.appendChild(regBusinessInput);
    }

    let regBusinessSubmit = document.createElement('input');
    regBusinessSubmit.classList.add();
    regBusinessSubmit.type = "submit";
    regBusinessSubmit.value = "Təsdiqlə";
    regBusinessSubmit.classList.add("registrationSubmit");
    regBusinessSubmit.classList.add("submitGradient");

    regBusinessForm.appendChild(regBusinessSubmit);


    // Collecting all element in a one direct
    regContainer.appendChild(regConHeader);
    regContainer.appendChild(regBusinessForm);

    // Add main element to body
    document.body.appendChild(regContainer);

    // Cofirm market details
    regBusinessSubmit.addEventListener('click', () => {
        // Fill data to json or something
        alert("Finally good!");
    });
}


let sumbitBtn = document.querySelector('.registrationSubmit');

sumbitBtn.addEventListener('click', () => {
    
    let dropDownMenu = document.querySelector('.regAccountType');

    let selectedOption = dropDownMenu.options[dropDownMenu.selectedIndex].text; 

    if(selectedOption == "Satıcı") {
        document.querySelector('.registrationContainer').remove();
        createBusinessAccount();
    }
    else {
        // Submit the form
    }
   
});